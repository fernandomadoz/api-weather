
const { Router } = require('express');
const router = Router();
const request = require('request');
const ipapi = require('ipapi.co');
const weather = require('openweather-apis');
const axios = require('axios');

let weatherAPPID = '5cc550cd63b1fcc06ac1e20f10b11540';

async function callApi(url, res){ 
  return await new Promise((resolve, reject)=>{

    axios.get(url).then(response => {
      let location = response.data;
      //console.log(ip2 );
      res.json(location);
    })
    .catch(error => {
      console.log(error);
    });
  })
}

function theip(ip) {
  if ( ip == '::ffff:127.0.0.1' || ip == '127.0.0.1') {
    ip = '8.8.8.8';
  }
  else {
    ip = req.ip;
  }
  return ip
}

router.get('/v1/location', (req, res) => {

  let ip = theip(req.ip);
  let url = 'https://ipapi.co/'+ip+'/json/';
  callApi(url, res).then(result =>{
      console.log(result);
    });


})


router.get('/v1/current/:city?', (req, res) => {
  var city = req.params.city;
  let ip = theip(req.ip);
    if (!city) {


      async function myPromise(){ 
        return await new Promise((resolve, reject)=>{
    
          axios.get('https://ipapi.co/'+ip+'/json/').then(response => {
            let location = response.data;

            async function myPromise2(){ 
              return await new Promise((resolve, reject)=>{
          
                axios.get('https://api.openweathermap.org/data/2.5/weather?lat='+location.latitude+'&lon='+location.longitude+'&appid='+weatherAPPID)
                .then(response => {
                  let weatheinfo = response.data;
                  
                  const mergedObject = {
                    ...location,
                    ...weatheinfo
                  };
                  console.log(mergedObject );
                  res.json(mergedObject);
                })
                .catch(error => {
                  console.log(error);
                });

              })
            }
          
            myPromise2().then(result =>{
              res.json(result);
            });


          })
          .catch(error => {
            console.log(error);
          });
        })
      }
    
      myPromise().then(result =>{
        res.json(result);
      });



    }
    else {
      res.json("no entendi cual es el metodo para determinar la ciudad usando ip-api");
    }

    /*

    */
})



router.get('/v1/forecast/:city?', (req, res) => {
  var city = req.params.city;
  let ip = theip(req.ip);
    if (!city) {


      async function myPromise(){ 
        return await new Promise((resolve, reject)=>{
    
          axios.get('https://ipapi.co/'+ip+'/json/').then(response => {
            let location = response.data;

            async function myPromise2(){ 
              return await new Promise((resolve, reject)=>{
          
                axios.get('https://api.openweathermap.org/data/2.5/forecast?lat='+location.latitude+'&lon='+location.longitude+'&appid='+weatherAPPID)
                .then(response => {
                  let weatheinfo = response.data;
                  
                  const mergedObject = {
                    ...location,
                    ...weatheinfo
                  };
                  console.log(mergedObject );
                  res.json(mergedObject);
                })
                .catch(error => {
                  console.log(error);
                });

              })
            }
          
            myPromise2().then(result =>{
              res.json(result);
            });


          })
          .catch(error => {
            console.log(error);
          });
        })
      }
    
      myPromise().then(result =>{
        res.json(result);
      });



    }
    else {
      res.json("no entendi cual es el metodo para determinar la ciudad usando ip-api o weathermap");
    }

    /*
    weather.getTemperature(function(err, temp){
      console.log(temp);
    });
    */
})

module.exports = router;