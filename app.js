const express = require('express')
const morgan = require('morgan')
const app = express()
const port = 3000




//settings 
app.set('port', process.env.PORT || port);
app.set('json spaces', 2);

//routes
app.use(require('./src/routes/routes'));



//middleware
app.use(morgan('dev'));
app.use(express.urlencoded({extended:false}));
app.use(express.json);


//Starting the server
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })

  